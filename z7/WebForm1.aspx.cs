﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace z7
{
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void ButtonSend_Click(object sender, EventArgs e)
		{
			ResultLabel.Text = "Hello World!";
		}

		protected void year_SelectedIndexChanged(object sender, EventArgs e)
		{
			ResultLabel.Text = year.SelectedValue;
		}

		protected void remember_CheckedChanged(object sender, EventArgs e)
		{
			if (remember.Checked)
			{
				ResultLabel.Text = remember.Checked.ToString();
			}
			else
			{
				ResultLabel.Text = "";
			}
		}
	}
}