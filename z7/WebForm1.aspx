﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="z7.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<asp:Label ID="LoginLabel" runat="server" Text="Логин"></asp:Label>
			<br>
			<asp:TextBox ID="login" runat="server"></asp:TextBox>
		</div>
		<div>
			<asp:Label ID="EmailLabel" runat="server" Text="Email"></asp:Label>
			<br>
			<asp:TextBox ID="email" runat="server"></asp:TextBox>
		</div>
		<div>
			<asp:Label ID="YaerLabel" runat="server" Text="Год"></asp:Label>
			<br>
			<asp:DropDownList id="year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="year_SelectedIndexChanged">
				<asp:ListItem>1941</asp:ListItem>
				<asp:ListItem>1945</asp:ListItem>
			</asp:DropDownList>
		</div>
		<div>
			<asp:CheckBox ID="remember" runat="server" AutoPostBack="True" OnCheckedChanged="remember_CheckedChanged" />
			<asp:Label ID="RememberLabel" runat="server" Text="Запомнить"></asp:Label>
		</div>
		<div>
			<asp:Button ID="ButtonSend" runat="server" Text="Отправить" OnClick="ButtonSend_Click" />
		</div>
		<div>
			<asp:Label ID="ResultLabel" runat="server" Text=""></asp:Label>
		</div>
	</form>
</body>
</html>
