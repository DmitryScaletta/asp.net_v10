﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="z9.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
		<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
			<ContentTemplate>
				<div>
					<asp:Button ID="ButtonSend" runat="server" Text="Отправить" OnClick="ButtonSend_Click" />
				</div>
				<div>
					<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="ButtonSend" />
			</Triggers>
		</asp:UpdatePanel>

		<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
			<ContentTemplate>
				<div>
					<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="1000"></asp:Timer>
					<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="Timer1" />
			</Triggers>
		</asp:UpdatePanel>

		<asp:SqlDataSource
			ID="SqlDataSource1"
			runat="server"
			SelectCommand="SELECT [carBrand] FROM [CarBrands]"
			ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
		></asp:SqlDataSource>
		<asp:SqlDataSource
			ID="SqlDataSource2"
			runat="server"
			SelectCommand="SELECT [model] FROM [Models] WHERE ([carBrand] = @carBrand)"
			ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
		>
			<SelectParameters>
				<asp:ControlParameter ControlID="DropDownList1" Name="carBrand" PropertyName="SelectedValue" Type="String" />
			</SelectParameters>
		</asp:SqlDataSource>

		<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
			<ContentTemplate>
				<div>
					<asp:Label ID="Label1" runat="server" Text="Car Brand"></asp:Label>
					<br>
					<asp:DropDownList
						ID="DropDownList1"
						runat="server"
						AutoPostBack="True"
						DataSourceID="SqlDataSource1"
						DataTextField="carBrand"
						DataValueField="carBrand"
					></asp:DropDownList>
				</div>
				<div>
					<asp:Label ID="Label2" runat="server" Text="Model"></asp:Label>
					<br>
					<asp:DropDownList
						ID="DropDownList2"
						runat="server"
						DataSourceID="SqlDataSource2"
						DataTextField="model"
						DataValueField="model"
					></asp:DropDownList>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="DropDownList1" />
			</Triggers>
		</asp:UpdatePanel>
	</form>
</body>
</html>
