CREATE TABLE [dbo].[Models] (
    [id]       NCHAR (10) NOT NULL,
    [carBrand] NCHAR (20) NULL,
    [model]    NCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[CarBrands] (
    [id]       NCHAR (10) NOT NULL,
    [carBrand] NCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

INSERT INTO CarBrands VALUES (1, 'Ford');
INSERT INTO CarBrands VALUES (2, 'Toyota');
INSERT INTO CarBrands VALUES (3, 'Citroen');

INSERT INTO Models VALUES (1,  'Ford', 'Focus');
INSERT INTO Models VALUES (2,  'Ford', 'Fiesta');
INSERT INTO Models VALUES (3,  'Ford', 'Mustang');
INSERT INTO Models VALUES (4,  'Toyota', 'Camri');
INSERT INTO Models VALUES (5,  'Toyota', 'Corolla');
INSERT INTO Models VALUES (6,  'Toyota', 'Tundra');
INSERT INTO Models VALUES (7,  'Citroen', 'c3');
INSERT INTO Models VALUES (8,  'Citroen', 'c4');
INSERT INTO Models VALUES (9,  'Citroen', 'c5');
INSERT INTO Models VALUES (10, 'Citroen', 'Berlingo');
