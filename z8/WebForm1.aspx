﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="z8.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<div>
				<asp:Label ID="LabelName" runat="server" Text="Ваше имя"></asp:Label>
				<asp:RequiredFieldValidator
					ID="NameRequiredFieldValidator"
					runat="server" ControlToValidate="name"
					ErrorMessage="Укажите имя"
					Text="*"
					Display="dynamic"
				></asp:RequiredFieldValidator>
				<br>
				<asp:TextBox ID="name" runat="server"></asp:TextBox>
			</div>
			<div>
				<asp:Label ID="LabelPhone" runat="server" Text="Телефон"></asp:Label>
				<asp:RequiredFieldValidator
					ID="PhoneRequiredFieldValidator"
					runat="server"
					ControlToValidate="phone"
					ErrorMessage="Укажите телефон"
					Text="*"
					Display="dynamic"
				></asp:RequiredFieldValidator>
				<br>
				<asp:TextBox ID="phone" runat="server"></asp:TextBox>
			</div>
			<div>
				<asp:Label ID="LabelEmail" runat="server" Text="Email"></asp:Label>
				<asp:RequiredFieldValidator 
					ID="EmailRequiredFieldValidator"
					runat="server"
					ControlToValidate="email"
					ErrorMessage="Укажите Email"
					Text="*"
					Display="dynamic"
				></asp:RequiredFieldValidator>
				<asp:RegularExpressionValidator 
					ID="EmailFieldValidator" 
					runat="server" 
					ControlToValidate="email" 
					ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
					ErrorMessage="E-mail введен не корректно"
					Text="*" 
					Display="dynamic"
				></asp:RegularExpressionValidator>
				<br>
				<asp:TextBox ID="email" runat="server"></asp:TextBox>
			</div>
			<div>
				<asp:Button ID="ButtonSend" runat="server" Text="Отправить" OnClick="ButtonSend_Click" />
			</div>
			<div>
				<asp:Label ID="LabelResult" runat="server" Text=""></asp:Label>
			</div>
			<div>
				<asp:ValidationSummary ID="ValidationSummary" runat="server" />
			</div>
		</div>
	</form>
</body>
</html>
